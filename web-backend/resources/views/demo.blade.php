<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>iReload Engine Software Pulsa</title>
  <link href="{{ URL::asset('dist_native/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{ URL::asset('dist_native/css/heroic-features.css')}}" rel="stylesheet">
  <script src="{{ URL::asset('dist_native/assets/custom-apk.js')}}" type="text/javascript"></script>
</head>

<body>
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="#">iReload Engine Software Pulsa Customize Android</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

    </div>
    </div>
</nav>

<div class="container">

    <header class="jumbotron my-4">

        <!-- <form action="/iReloadEngineCustomMobile/apk-build" method="post" enctype="multipart/form-data"> -->
            <div class="form-group">
                <label for="exampleInputEmail1">ID Aplikasi</label>
                <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="* udiencell" name="app_id" required="true" value="id.co.ie.demo.test.celular" disabled="true">
                <small id="emailHelp" class="form-text text-muted">Tidak mengandung huruf besar dan spasi, hanya huruf kecil. Contoh : udiencell</small>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Nama Aplikasi</label>
                <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Udein Celular" name="app_name" required="true" value="User Demo Celular" disabled="true">
                <input type="hidden" name="app_server_addr" value="https://mob.icg.co.id/msie_iesp/">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Icon Aplikasi</label>
                <input type="file" name="app_image_src" class="form-control-file" id="exampleFormControlFile1" required="true">
                <small id="emailHelp" class="form-text text-muted">Ukuran icon harus 512x512</small>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Color Primary</label>
                <input type="color" name="color_light" value="#4CAF50" id="colorPrimary">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Color Primary Dark</label>
                <input type="color" name="color_dark" value="#388E3C" id="colorPrimaryDark">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Color Button</label>
                <input type="color" name="color_accent" value="#FFC107" id="colorPrimaryButton">
            </div>
            <!-- <button type="submit" class="btn btn-primary">Buat APK</button> -->
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                Buat APK
            </button>

            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Pemberitahuan</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            Apakah anda yakin data yang ada input sudah siap untuk membuat APK Android sekarang?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
                            <a href="http://www.ie.co.id/order/business/"><button type="button" class="btn btn-primary">Ya</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </form>


    </header>

    <div class="row text-center">

        <div class="col-lg-3 col-md-6 mb-4">
            <div class="card" style="margin: 0px; padding: 0px; border:none; background: green">
                <img id="color-dark-c1" style="background: #388E3C" class="card-img-top" src="/storage/img/custom1-part1.png" alt="">
                <img id="color-primary-c1" style="background: #4CAF50" class="card-img-bottom" src="/storage/img/custom1-part2.png" alt="">

            </div>
        </div>

        <div class="col-lg-3 col-md-6 mb-4">
            <div class="card"  style="margin: 0px; padding: 0px; border:none; background: green">
                <img id="color-primary-c2-p1" style="background: #4CAF50" class="card-img-top" src="/storage/img/custom2-part1.png" alt="">
                <img id="color-button" class="card-img-top" style="background: #FFC107;" src="/storage/img/custom2-part2.png" alt="">
                <img id="color-primary-c2-p2" style="background: #4CAF50" class="card-img-top" src="/storage/img/custom2-part3.png" alt="">
            </div>
        </div>

        <div class="col-lg-3 col-md-6 mb-4">
            <div class="card" style="margin: 0px; padding: 0px; border:none; background: green">
                <img id="color-dark-c3" style="background: #388E3C" class="card-img-top" src="/storage/img/custom1-part1.png" alt="">
                <img id="color-primary-c3" style="background: #4CAF50" class="card-img-bottom" src="/storage/img/custom3.png" alt="">

            </div>
        </div>



    </div>

</div>

<footer class="py-5 bg-dark">
    <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; iReload Engine Customize Android</p>
    </div>
</footer>

  <script src="{{ URL::asset('dist_native/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{ URL::asset('dist_native/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

</body>

</html>
