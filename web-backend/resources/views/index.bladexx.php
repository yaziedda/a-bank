<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>iReload Engine Software Pulsa</title>
  <link href="{{ URL::asset('dist_native/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{ URL::asset('dist_native/css/heroic-features.css')}}" rel="stylesheet">
  <script src="{{ URL::asset('dist_native/assets/custom-apk.js')}}" type="text/javascript"></script>
</head>

<body>
  <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="#">iReload Engine Software Pulsa Customize Mobile App</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="/dashboard">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/build-apk">Build APK</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/list-apk">List APK</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/logout">Logout</a>
        </li>
      </ul>
    </div>
  </nav>

  <br><br>

  <div class="container">
    <div class="row">
      <div class="col-md-3">
        <h2>Registrasion ID</h2>
        <p><a class="btn btn-secondary" href="#" role="button">{{$model['user']->id}}</a></p>
      </div>
      <div class="col-md-3">
        <h2>App ID</h2>
        <p><a class="btn btn-danger" href="#" role="button">{{$model['user']->app_id}}</a></p>
      </div>
      @if($model['mobile_builder'] != null)
      <div class="col-md-3">
        <h2>App Name</h2>
        <p>
          <a class="btn btn-warning" href="#" role="button">
            {{$model['mobile_builder']->app_name}}
          </a>
        </p>
      </div>
      <div class="col-md-3">
        <h2>Link</h2>
        @if($model['mobile_builder']->apk_link != null && $model['mobile_builder']->status == 1)
        <p><a class="btn btn-success" href="apk/{{$model['mobile_builder']->apk_link}}" role="button">Download</a></p>
        @else
        <p><a class="btn btn-warning" role="button">APK Sedang diproses</a></p>
        @endif
      </div>
      @endif
    </div>
    <hr>
  </div>

  <div class="container">

    @if($model['mobile_builder'] == null)
    <div class="jumbotron">
      <h1>Belum ada APK yang terbuat</h1> 
      <p><a href="/build-apk">Klik untuk membuat APK</a></p> 
    </div>
    <br><br><br><br><br><br><br><br><br><br><br>
    @else
    <br><br>
    <div class="row text-center">

      <div class="col-lg-3 col-md-6 mb-4">
        <div class="card" style="margin: 0px; padding: 0px; border:none; background: green">
          <img id="color-dark-c1" 
          @if($model['mobile_builder'] != null)
          style="background: {{$model['mobile_builder']->color_primary_dark}}" 
          @else
          style="background: #388E3C" 
          @endif
          class="card-img-top" src="{{ URL::asset('dist_native/assets/img/custom1-part1.png')}}" alt="">
          <img id="color-primary-c1" 
          @if($model['mobile_builder'] != null)
          style="background: {{$model['mobile_builder']->color_primary}}" 
          @else
          style="background: #4CAF50" 
          @endif
          class="card-img-bottom" src="{{ URL::asset('dist_native/assets/img/custom1-part2.png')}}" alt="">

        </div>
      </div>

      <div class="col-lg-3 col-md-6 mb-4">
        <div class="card"  style="margin: 0px; padding: 0px; border:none; background: green">
          <img id="color-primary-c2-p1" 
          @if($model['mobile_builder'] != null)
          style="background: {{$model['mobile_builder']->color_primary}}" 
          @else
          style="background: #4CAF50" 
          @endif
          class="card-img-top" src="{{ URL::asset('dist_native/assets/img/custom2-part1.png')}}" alt="">
          <img id="color-button" class="card-img-top" 
          @if($model['mobile_builder'] != null)
          style="background: {{$model['mobile_builder']->color_primary_button}}" 
          @else
          style="background: #FFC107;" 
          @endif
          src="{{ URL::asset('dist_native/assets/img/custom2-part2.png')}}" alt="">
          <img id="color-primary-c2-p2" 
          @if($model['mobile_builder'] != null)
          style="background: {{$model['mobile_builder']->color_primary}}" 
          @else
          style="background: #4CAF50"
          @endif
          class="card-img-top" src="{{ URL::asset('dist_native/assets/img/custom2-part3.png')}}" alt="">
        </div>
      </div>

      <div class="col-lg-3 col-md-6 mb-4">
        <div class="card" style="margin: 0px; padding: 0px; border:none; background: green">
          <img id="color-dark-c3" 
          @if($model['mobile_builder'] != null)
          style="background: {{$model['mobile_builder']->color_primary_dark}}" 
          @else
          style="background: #388E3C"
          @endif
          class="card-img-top" src="{{ URL::asset('dist_native/assets/img/custom1-part1.png')}}" alt="">
          <img id="color-primary-c3" 
          @if($model['mobile_builder'] != null)
          style="background: {{$model['mobile_builder']->color_primary}}" 
          @else
          style="background: #4CAF50" 
          @endif
          class="card-img-bottom" src="{{ URL::asset('dist_native/assets/img/custom3.png')}}" alt="">

        </div>
      </div>  
    </div>
    @endif

    <br><br>

  </div>



  <footer class="py-5 bg-dark">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright &copy; iReload Engine Customize Android</p>
    </div>
  </footer>

  <script src="{{ URL::asset('dist_native/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{ URL::asset('dist_native/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

</body>

</html>
