<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>iReload Engine Software Pulsa</title>
  <link href="{{ URL::asset('dist_native/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{ URL::asset('dist_native/css/heroic-features.css')}}" rel="stylesheet">
  <!-- <script src="{{ URL::asset('dist_native/assets/custom-apk.js')}}" type="text/javascript"></script> -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
  <script>
    $( document ).ready(function() {
      console.log( "ready!" );
      // document.getElementById("app_image_src_thumbnail").style.visibility = "hidden";
      function readURL(input) {

        if (input.files && input.files[0]) {
          var reader = new FileReader();

          reader.onload = function(e) {
            var image = new Image();
            image.onload = function(evt) {
              var width = this.width;
              var height = this.height;
              console.log('width : '+width);
              console.log('height : '+height);

              if(width >= 512 && height >= 512){
                document.getElementById("app_image_src_thumbnail").style.visibility = "visible";
                $('#app_image_src_thumbnail').attr('src', e.target.result);
              }else{
                document.getElementById("app_image_src_thumbnail").style.visibility = "hidden";
                $('#app_image_src_thumbnail').attr('src', null);
                document.getElementById("app_image_src").value = "";
                alert('Ukuran gambar harus lebih dari atau sama dengan 512 x 512');
              }
            };
            image.src = e.target.result; 

          }
          reader.readAsDataURL(input.files[0]);
        }else{
          console.log('noob');
        }
      }

      $("#app_image_src").change(function() {
        console.log('okok');
        readURL(this);
      });

      $('#app_id').on('keyup', function() {
       if (this.value.length > 0) {
        var text = this.value;
        this.value = text.toLowerCase();
        if (this.value.match(/[^a-zA-Z]/g)) {
         $(this).val(this.value.replace(/[^a-zA-Z]/g, ''));
       }
       console.log(text);
     }
   });
      $('#app_id').on('keypress', function(e) {
        if (e.which == 32)
          return false;
      });
    });

  </script>
</head>

<body>
  <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="#">iReload Engine Software Pulsa Customize Mobile App</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="/dashboard">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/build-apk">Build APK </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/list-apk">List APK</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/logout">Logout</a>
        </li>
      </ul>
    </div>
  </nav>

  <br><br>

  <div class="container">
    <!-- Example row of columns -->
    <div class="row">

      <div class="col-md-3">
        <h2>Registrasion ID</h2>
        <p><a class="btn btn-secondary" href="#" role="button">{{$model['user']->id}}</a></p>
      </div>
      @if($model['user']->app_id != null)
      <div class="col-md-3">
        <h2>App ID</h2>
        <p><a class="btn btn-danger" href="#" role="button">{{$model['user']->app_id}}</a></p>
      </div>
      @endif
      @if($model['mobile_builder'] != null)
      <div class="col-md-3">
        <h2>App Name</h2>
        <p>
          <a class="btn btn-warning" href="#" role="button">
            {{$model['mobile_builder']->app_name}}
          </a>
        </p>
      </div>
      <div class="col-md-3">
        <h2>Link</h2>
        @if($model['mobile_builder']->apk_link != null && $model['mobile_builder']->status == 1)
        <p><a class="btn btn-success" href="{{$model['mobile_builder']->apk_link}}" role="button">Download</a></p>
        @else
        <p><a class="btn btn-warning" role="button">APK Sedang diproses</a></p>
        @endif
      </div>
      @endif
    </div>

    <hr>

  </div>

  <div class="container">

    @if(Session::has('alert-success'))
    <div class="alert alert-success">
      {{ Session::get('alert-success') }}
    </div>
    @endif

    @if(Session::has('alert-danger'))
    <div class="alert alert-danger">
      {{ Session::get('alert-danger') }}
    </div>
    @endif

    @if($model['user']->status == 1)
    <div class="alert alert-danger" role="alert">      
      <h3>Konfigurasi ID Aplikasi.</h3>
    </div>
    @endif

    <header class="jumbotron my-4">

      <form action="/config" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="form-group">
          <label for="exampleInputEmail1">ID Aplikasi</label>
          @if($model['user']->status == 1)
          <input type="text" class="form-control" id="app_id" aria-describedby="emailHelp" placeholder="* udiencell" name="app_id" required="true">
          <div style="color: red; font-size: 12px; margin-top: 5px;">* tidak boleh mengandung angka, huruf besar dan spesial karakter, hanya huruf kecil yang diperbolehkan untuk app id</div>
          @elseif($model['user']->status == 2)
          <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="* udiencell" name="app_id" required="true" value="{{$model['user']->app_id}}" disabled="true">
          @endif
        </div>
        <!-- <div class="form-group">
          <label for="exampleInputEmail1">Nama Aplikasi</label>
          @if($model['user']->status == 1)
          <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Contoh : Udein Celular" name="app_name" required="true" name="app_name" >
          @elseif($model['user']->status == 2)
          <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Contoh : Udein Celular" name="app_name" required="true" name="app_name" value="{{$model['user']->app_name}}" disabled="true">
          @endif
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Icon Aplikasi</label><br>
          @if($model['user']->status == 1)
          <input type="file" id="app_image_src" name="app_image_src" class="form-control-file" required="true" accept="image/*" >
          <img src="#" alt="thumbnail" id="app_image_src_thumbnail" style="max-width: 200px; visibility: hidden;">
          <small id="emailHelp" class="form-text text-muted">Ukuran icon harus 512x512</small>
          @elseif($model['user']->status == 2)
          <img src="/storage/icon/{{$model['user']->icon}}" alt="thumbnail" id="app_image_src_thumbnail" style="max-width: 200px">
          @endif
        </div> -->
        @if($model['user']->status == 1)
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
          SIMPAN
        </button>
        @endif
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Pemberitahuan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                Apakah anda yakin data yang ada input sudah siap untuk membuat APK Android sekarang? 
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
                <button type="submit" class="btn btn-primary">Ya</button>
              </div>
            </div>
          </div>
        </div>
      </form>

      @if($model['user']->status == 2)
      <br><br>
      <h1>Akun VPN Anda</h1>
      <div class="form-group">
        <label for="exampleInputEmail1">Host</label>
        <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" required="true" disabled="true" value="{{$model['vpn']->host}}">
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">IP</label>
        <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" required="true" disabled="true" value="{{$model['vpn']->ip}}">
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Username</label>
        <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="* udiencell"  required="true" disabled="true" value="{{$model['vpn']->username}}">
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Password</label>
        <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="* udiencell"  required="true" disabled="true" value="{{$model['vpn']->password}}">
      </div>
      @endif


    </header>

    

  </div>

  <footer class="py-5 bg-dark">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright &copy; iReload Engine Customize Android</p>
    </div>
  </footer>

  <script src="{{ URL::asset('dist_native/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{ URL::asset('dist_native/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

</body>

</html>