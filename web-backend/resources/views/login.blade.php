<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>A-BANK ( Antrian bank online )</title>
  <link href="{{ URL::asset('dist_native/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{ URL::asset('dist_native/css/heroic-features.css')}}" rel="stylesheet">
  <script src="{{ URL::asset('dist_native/assets/custom-apk.js')}}" type="text/javascript"></script>
  <link href="{{ URL::asset('dist_native/css/floating-labels.css')}}" rel="stylesheet">
</head>

<body>
  <form class="form-signin" action="/login-user" method="post">
    {{csrf_field()}}
    @if(Session::has('alert-success'))
    <div class="alert alert-success">
      {{ Session::get('alert-success') }}
    </div>
    @endif

    @if(Session::has('alert-danger'))
    <div class="alert alert-danger">
      {{ Session::get('alert-danger') }}
    </div>
    @endif
    <div class="text-center mb-4">
      <!-- <img class="mb-4" src="{{ URL::asset('dist_native/assets/img/iesp.png')}}" alt="" width="200" height="200"> -->
      <h1 class="h3 mb-3 font-weight-normal">A-BANK ( Antrian bank online )</h1>
    </div>

    <div class="form-label-group">
      <input type="text" id="inputEmail" class="form-control" placeholder="Email address" required="" autofocus="" name="user_id">
      <label for="inputEmail">User ID</label>
    </div>

    <div class="form-label-group">
      <input type="password" id="inputPassword" class="form-control" placeholder="Password" required="" name="password">
      <label for="inputPassword">Password</label>
    </div>
    <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
    <!-- <p class="text-muted text-center" style="margin-top: 10px">Belum Punya Akun? <a href="/register">Daftar</a></p> -->
    <p class="mt-5 mb-3 text-muted text-center">© 2018 UBP STRUGGLE TEAM</p>
  </form>

  <script src="{{ URL::asset('dist_native/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{ URL::asset('dist_native/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

</body>

</html>
