@extends('index')
@section('content')
<!-- Bread crumb -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-primary">Dashboard</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->

    <!-- Container fluid  -->
    <div class="container-fluid">

        <p>
          <!-- <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
            Input Data
        </a> -->
    </p>

    <div class="row" >
        <div class="col-lg-12">
            <div class="card">
                <div class="card-title">
                </div>
                <div class="card-body">
                    <div class="sl-item">
                        <div class="sl-left"> 
                            <!-- <img src="images/users/avatar-2.jpg" alt="user" class="img-circle"> </div> -->
                            <div class="sl-right">
                                <div> 
                                    <a href="#" class="link"><b>#{{$model['data']->id}}</b></a>
                                    <div class="m-t-20 row">
                                        <div class="col-md-3 col-xs-12 text-center">
                                            <h1 style="font-size: 48px">{{$model['data']->code_booking}}</h1>
                                            <h1 style="font-size: 36px; color: red">00:00:12</h1>
                                        </div>
                                        <div class="col-md-9 col-xs-12">
                                            <p>Estimate at <span class="sl-date">{{$model['data']->time}}</span></p> 
                                            <form>
                                                <div class="form-group">
                                                    <label for="exampleInputuname">Nama</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" id="exampleInputuname" placeholder="Nama Pemohon" name="judul" required="">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputuname">Layanan</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" id="exampleInputuname" placeholder="Layanan" name="judul" required="">
                                                    </div>
                                                </div>
                                                <!-- <div class="form-group">
                                                    <label for="exampleInputuname">No Reg</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" id="exampleInputuname" placeholder="No Reg" name="judul" required="">
                                                    </div>
                                                </div> -->
                                            </form>
                                            <a href="/service-done/{{$model['data']->id}}" class="btn btn-danger"> Selesai & Simpan</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->
    @endsection