@extends('index')
@section('content')
<!-- Bread crumb -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-primary">Dashboard</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->

    <!-- Container fluid  -->
    <div class="container-fluid">

        <p>
          <!-- <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
            Input Data
        </a> -->
    </p>

    <div class="row" >
        <div class="col-lg-12">
            <div class="card">
                <div class="card-title">
                </div>
                <div class="card-body">
                    <div class="sl-item">
                        <div class="sl-left"> 
                            <!-- <img src="images/users/avatar-2.jpg" alt="user" class="img-circle"> </div> -->
                            <div class="sl-right">
                                <div> 
                                    @if($model['data'] != null)
                                    <a href="#" class="link"><b>#{{$model['data']->id}}</b></a>
                                    <div class="m-t-20 row">
                                        <div class="col-md-3 col-xs-12 text-center">
                                            <h1 style="font-size: 48px">{{$model['data']->code_booking}}</h1>
                                        </div>
                                        <div class="col-md-9 col-xs-12">
                                            <p>Estimate at <span class="sl-date">{{$model['data']->time}}</span></p> 
                                            @if($model['data']->arrived == 0)
                                            <p><span class="btn btn-danger">NOT VERIFIED</span></p>
                                            @else
                                            <p><span class="btn btn-success">VERIFIED</span></p>
                                            @endif
                                            <a href="/call/{{$model['data']->id}}" class="btn btn-success"> Panggil</a> 
                                            <a href="/service/{{$model['data']->id}}" class="btn btn-warning"> Layani</a> 
                                            <a href="/service-skip/{{$model['data']->id}}" class="btn btn-danger"> Lewati</a>
                                        </div>
                                    </div>
                                    @else
                                    <div class="col-md-12 col-xs-12 text-center">
                                        <h1 style="font-size: 48px">
                                            TIDAK ADA ANTRIAN
                                        </h1>
                                    </div>
                                    @endif
                                    <div class="like-comm m-t-20">
                                        @if(isset($model['dataList']))
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Code</th>
                                                    <th>Time Estimate</th>
                                                    <th></th>
                                                    <th></th>
                                                    <th>Verified</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                @foreach($model['dataList'] as $item)
                                                <tr>
                                                    <td>{{$item->id}}</td>
                                                    <td>{{$item->code_booking}}</td>
                                                    <td><b>{{$item->time}}</b></td>
                                                    <td><b>{{$item->diff}}</b></td>
                                                    <td>
                                                        @if($item->status == 0)
                                                        <span class="btn btn-default">MENUNGGU</span>
                                                        @elseif($item->status == 1)
                                                        <span class="btn btn-danger">SELESAI</span>
                                                        @elseif($item->status == 2)
                                                        <span class="btn btn-warning">SEDANG DALAM LAYANAN</span>
                                                        @elseif($item->status == 3)
                                                        <span class="btn btn-danger">TERLEWAT</span>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if($item->arrived == 0)
                                                        <span class="btn btn-danger">NO</span>
                                                        @else
                                                        <span class="btn btn-success">YES</span>
                                                        @endif
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        @endif
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->
    @endsection