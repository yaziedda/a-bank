<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>iReload Engine Software Pulsa</title>
  <link href="{{ URL::asset('dist_native/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{ URL::asset('dist_native/css/heroic-features.css')}}" rel="stylesheet">
  <script src="{{ URL::asset('dist_native/assets/custom-apk.js')}}" type="text/javascript"></script>
  <link href="{{ URL::asset('dist_native/css/floating-labels.css')}}" rel="stylesheet">
</head>

<body>
  <form class="form-signin">
    <div class="text-center mb-4">
      <img class="mb-4" src="{{ URL::asset('dist_native/assets/img/iesp.png')}}" alt="" width="200" height="200">
      <h1 class="h3 mb-3 font-weight-normal">iReload Engine Software Pulsa Customize Mobile App</h1>
    </div>

    <div class="form-label-group">
      <input type="email" id="inputEmail" class="form-control" placeholder="Email address" required="" autofocus="">
      <label for="inputEmail">Email address</label>
    </div>

    <div class="form-label-group">
      <input type="email" id="inputEmail" class="form-control" placeholder="Email address" required="" autofocus="">
      <label for="inputEmail">Email address</label>
    </div>

    <div class="form-label-group">
      <input type="email" id="inputEmail" class="form-control" placeholder="Email address" required="" autofocus="">
      <label for="inputEmail">Email address</label>
    </div>

    <div class="form-label-group">
      <input type="password" id="inputPassword" class="form-control" placeholder="Password" required="">
      <label for="inputPassword">Password</label>
    </div>
    <button class="btn btn-lg btn-primary btn-block" type="submit">Sign up</button>
    <p class="text-muted text-center" style="margin-top: 10px">Sudah Punya Akun? <a href="/login">Login</a></p>
    <p class="mt-5 mb-3 text-muted text-center">© iReload Engine Software Pulsa</p>
  </form>

  <script src="{{ URL::asset('dist_native/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{ URL::asset('dist_native/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

</body>

</html>
