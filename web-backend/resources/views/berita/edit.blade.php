@extends('index')
@section('content')
<!-- Bread crumb -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-primary">Dashboard</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->

    <!-- Container fluid  -->
    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Edit Berita</h4>
                        <h6 class="card-subtitle">isi dengan data yang benar</h6>
                        <form action="{{route('berita.update', $model->id)}}" method="post" enctype="multipart/form-data">
                            <input name="_method" type="hidden" value="PATCH">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="exampleInputuname">Judul</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="exampleInputuname" placeholder="Judul" name="judul" required="" value="{{$model->judul}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputuname">Gambar</label>
                                <div class="input-group">
                                    <input type="file" class="form-control" id="exampleInputuname"  name="gambar" >
                                </div>
                                <br><img src="/storage/{{$model->gambar}}" width="200">
                            </div>
                            <div class="form-group">
                                <textarea class="textarea_editor form-control" name="content" rows="15" placeholder="Enter text ..." style="height:450px">{{$model->content}}</textarea>
                            </div>
                            <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                            <a href="/admin/berita"><button type="button" class="btn btn-info waves-effect waves-light m-r-10">Back</button></a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End PAge Content -->
</div>
<!-- End Container fluid  -->
@endsection