@extends('index')
@section('content')
<!-- Bread crumb -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-primary">Dashboard</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->

    <!-- Container fluid  -->
    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Input User</h4>
                        <h6 class="card-subtitle">isi dengan data yang benar</h6>
                        <form action="{{route('user.update', $model->id)}}" method="post" enctype="multipart/form-data">
                            <input name="_method" type="hidden" value="PATCH">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="exampleInputuname">Nama</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="exampleInputuname" placeholder="Nama" name="nama" required="" value="{{$model->nama}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputuname">No HP</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="exampleInputuname" placeholder="No HP" name="msisdn" required="" value="{{$model->msisdn}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputuname">NIM</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="exampleInputuname" placeholder="NIM" name="nim" required="" value="{{$model->nim}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputuname">Prodi</label>
                                <select class="form-control custom-select" required="" name="prodi">
                                    
                                    <option value="{{$model->prodi}}">{{$model->prodi}}</option>
                                    <option>-- Pilih Prodi--</option>
                                    <option value="TEKNIK INFORMATIKA">TEKNIK INFORMATIKA</option>
                                    <option value="TEKNIK INDUSTRI">TEKNIK INDUSTRI</option>
                                    <option value="SISTEM INFORMASI">SISTEM INFORMASI</option>
                                    <option value="FARMASI">FARMASI</option>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                            <a href="/admin/user"><button type="button" class="btn btn-info waves-effect waves-light m-r-10">Back</button></a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End PAge Content -->
</div>
<!-- End Container fluid  -->
@endsection