<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>iReload Engine Software Pulsa</title>
  <link href="{{ URL::asset('dist_native/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{ URL::asset('dist_native/css/heroic-features.css')}}" rel="stylesheet">
  <script src="{{ URL::asset('dist_native/assets/custom-apk.js')}}" type="text/javascript"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
  <script>
    $( document ).ready(function() {
      console.log( "ready!" );
      // document.getElementById("app_image_src_thumbnail").style.visibility = "hidden";
      function readURL(input) {

        if (input.files && input.files[0]) {
          var reader = new FileReader();

          reader.onload = function(e) {
            var image = new Image();
            image.onload = function(evt) {
              var width = this.width;
              var height = this.height;
              console.log('width : '+width);
              console.log('height : '+height);

              if(width >= 512 && height >= 512){
                document.getElementById("app_image_src_thumbnail").style.visibility = "visible";
                $('#app_image_src_thumbnail').attr('src', e.target.result);
              }else{
                document.getElementById("app_image_src_thumbnail").style.visibility = "hidden";
                $('#app_image_src_thumbnail').attr('src', null);
                document.getElementById("app_image_src").value = "";
                alert('Ukuran gambar harus lebih dari atau sama dengan 512 x 512');
              }
            };
            image.src = e.target.result; 

          }
          reader.readAsDataURL(input.files[0]);
        }else{
          console.log('noob');
        }
      }

      $("#app_image_src").change(function() {
        console.log('okok');
        readURL(this);
      });

      $('#app_id').on('keyup', function() {
       if (this.value.length > 0) {
        var text = this.value;
        this.value = text.toLowerCase();
        if (this.value.match(/[^a-zA-Z]/g)) {
         $(this).val(this.value.replace(/[^a-zA-Z]/g, ''));
       }
       console.log(text);
     }
   });
      $('#app_name').on('keyup', function() {
       if (this.value.length > 0) {
        var text = this.value;
        document.getElementById("app_name_preview").innerHTML = text;

        console.log(text);
      }
    });
      $('#app_id').on('keypress', function(e) {
        if (e.which == 32)
          return false;
      });
    });

  </script>
</head>

<body>
  <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="#">iReload Engine Software Pulsa Customize Mobile App</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item">
          <a class="nav-link" href="/dashboard">Home</a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="/build-apk">Build APK </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/list-apk">List APK</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/logout">Logout</a>
        </li>
      </ul>
    </div>
  </nav>

  <br><br>

  <div class="container">
    <!-- Example row of columns -->
    <div class="row">

      <div class="col-md-3">
        <h2>Registrasion ID</h2>
        <p><a class="btn btn-secondary" href="#" role="button">{{$model['user']->id}}</a></p>
      </div>
      <div class="col-md-3">
        <h2>App ID</h2>
        <p><a class="btn btn-danger" href="#" role="button">{{$model['user']->app_id}}</a></p>
      </div>
      @if($model['mobile_builder'] != null)
      <div class="col-md-3">
        <h2>App Name</h2>
        <p>
          <a class="btn btn-warning" href="#" role="button">
            {{$model['mobile_builder']->app_name}}
          </a>
        </p>
      </div>
      <div class="col-md-3">
        <h2>Link</h2>
        @if($model['mobile_builder']->apk_link != null && $model['mobile_builder']->status == 1)
        <p><a class="btn btn-success" href="{{$model['mobile_builder']->apk_link}}" role="button">Download</a></p>
        @else
        <p><a class="btn btn-warning" role="button">APK Sedang diproses</a></p>
        @endif
      </div>
      @endif
    </div>

    <hr>

  </div>

  <div class="container">

    @if($model['mobile_builder'] == null)
    <!-- <div class="alert alert-danger" role="alert">      
      Pembuatan APK hanya bisa 1 kali, pastikan warna, nama dan icon anda sesuai pilihan.
    </div> -->
    @endif

    <div class="row">
      <div class="col-md-4">
        <header class="jumbotron">

          <form action="/generate" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
              <label for="exampleInputEmail1">ID Aplikasi</label>
              <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="* udiencell" name="app_id" required="true" disabled="true" value="{{$model['user']->app_id}}">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Nama Aplikasi</label>
              <!-- @if($model['user']->status == 2)
              <input type="text" class="form-control" id="app_name" aria-describedby="emailHelp" placeholder="Udein Celular" required="true">
              <input type="hidden" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Udein Celular" name="app_name" value="{{$model['user']->app_name}}">
              @else
              <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Contoh : Udein Celular" name="app_name" required="true" >
              @endif -->
              <input type="text" class="form-control" id="app_name" aria-describedby="emailHelp" placeholder="Contoh : Udein Celular" name="app_name" required="true" >
              <input type="hidden" name="app_server_addr" placeholder="https://mob.icg.co.id/mtg/">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Icon Aplikasi</label><br>


              <input type="file" name="app_image_src" class="form-control-file" id="app_image_src" required="true">
              <small id="emailHelp" class="form-text text-muted">Ukuran icon harus 512x512</small>
              <!-- example -->
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Color Primary</label>
              <input type="color" name="color_light" 
              @if($model['mobile_builder'] != null)
              value="{{$model['mobile_builder']->color_primary}}"
              @else
              value="#4CAF50"
              @endif
              id="colorPrimary">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Color Primary Dark</label>
              <input type="color" name="color_dark" 
              @if($model['mobile_builder'] != null)
              value="{{$model['mobile_builder']->color_primary_dark}}"
              @else
              value="#388E3C"
              @endif
              id="colorPrimaryDark">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Color Button</label>
              <input type="color" name="color_accent" 
              @if($model['mobile_builder'] != null)
              value="{{$model['mobile_builder']->color_primary_button}}"
              @else
              value="#FFC107"
              @endif
              id="colorPrimaryButton">
            </div>
            <button type="submit" class="btn btn-primary">Buat APK</button>
            <!-- Button trigger modal -->
            <!-- @if($model['mobile_builder'] != null && $model['mobile_builder']->status == 0) -->
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
              Buat APK
            </button>
            <!-- @else -->
        <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
          Buat kembali APK
        </button> -->
        <!-- @endif -->

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Pemberitahuan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                Apakah anda yakin data yang ada input sudah siap untuk membuat APK Android sekarang? 
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
                <button type="submit" class="btn btn-primary">Ya</button>
              </div>
            </div>
          </div>
        </div>
      </form>

    </header>
  </div>
  <div class="col-md-8">
    <div class="row text-center">

      <div class="col-lg-4 col-md-6 mb-4">
        <div class="card" style="margin: 0px; padding: 0px; border:none; background: green">
          <img id="color-dark-c1" 
          @if($model['mobile_builder'] != null)
          style="background: {{$model['mobile_builder']->color_primary_dark}}" 
          @else
          style="background: #388E3C" 
          @endif
          class="card-img-top" src="{{ URL::asset('dist_native/assets/img/custom1-part1.png')}}" alt="">
          <img id="color-primary-c1" 
          @if($model['mobile_builder'] != null)
          style="background: {{$model['mobile_builder']->color_primary}}" 
          @else
          style="background: #4CAF50" 
          @endif
          class="card-img-bottom" src="{{ URL::asset('dist_native/assets/img/custom1-part2.png')}}" alt="">

        </div>
      </div>

      <div class="col-lg-4 col-md-6 mb-4">
        <div class="card"  style="margin: 0px; padding: 0px; border:none; background: green">
          <div style="background: #f9f9f9; height: 125px;   line-height: 125px;text-align: center; padding-top: 10px">
            <img src="/storage/icon/default512.png" alt="thumbnail" id="app_image_src_thumbnail" style="max-height: 120px; visibility: visible; padding-bottom: 10px;">
          </div>
          <div style="background: #f9f9f9; padding-right: 16px; padding-left: 16px; padding-bottom: 10px;">
            <div style="font-size: 12px" id="app_name_preview">iReload Engine Software Pulsa</div>
          </div>
          <img id="color-primary-c2-p1" 
          @if($model['mobile_builder'] != null)
          style="background: {{$model['mobile_builder']->color_primary}}" 
          @else
          style="background: #4CAF50" 
          @endif
          class="card-img-top" src="{{ URL::asset('dist_native/assets/img/custom2-part1.png')}}" alt="">
          
          <img id="color-button" class="card-img" 
          @if($model['mobile_builder'] != null)
          style="background: {{$model['mobile_builder']->color_primary_button}}" 
          @else
          style="background: #FFC107;" 
          @endif
          src="{{ URL::asset('dist_native/assets/img/custom2-part2.png')}}" alt="">
          <img id="color-primary-c2-p2" 
          @if($model['mobile_builder'] != null)
          style="background: {{$model['mobile_builder']->color_primary}}" 
          @else
          style="background: #4CAF50"
          @endif
          class="card-img-top" src="{{ URL::asset('dist_native/assets/img/custom2-part3.png')}}" alt="">
        </div>
      </div>

      <div class="col-lg-4 col-md-6 mb-4">
        <div class="card" style="margin: 0px; padding: 0px; border:none; background: green">
          <img id="color-dark-c3" 
          @if($model['mobile_builder'] != null)
          style="background: {{$model['mobile_builder']->color_primary_dark}}" 
          @else
          style="background: #388E3C"
          @endif
          class="card-img-top" src="{{ URL::asset('dist_native/assets/img/custom1-part1.png')}}" alt="">
          <img id="color-primary-c3" 
          @if($model['mobile_builder'] != null)
          style="background: {{$model['mobile_builder']->color_primary}}" 
          @else
          style="background: #4CAF50" 
          @endif
          class="card-img-bottom" src="{{ URL::asset('dist_native/assets/img/custom3.png')}}" alt="">

        </div>
      </div>  
    </div>
  </div>
</div>





</div>

<footer class="py-5 bg-dark">
  <div class="container">
    <p class="m-0 text-center text-white">Copyright &copy; iReload Engine Customize Android</p>
  </div>
</footer>

<script src="{{ URL::asset('dist_native/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{ URL::asset('dist_native/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

</body>

</html>