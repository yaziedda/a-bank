@extends('index')
@section('content')
<!-- Bread crumb -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-primary">Dashboard</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->

    <!-- Container fluid  -->
    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-title">
                        <h4>Result </h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Permohonan</th>
                                        <th>Nama</th>
                                        <th>NIM</th>
                                        <th>Program Studi</th>
                                        <th>Status</th>
                                        <th>Created At</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach($model['data'] as $item)
                                    <tr>
                                        <td>{{$item->id}}</td>
                                        <td>{{$item->nama_permohonan}}</td>
                                        <td>{{$item->nama_user}}</td>
                                        <td>{{$item->nim}}</td>
                                        <td>{{$item->prodi}}</td>
                                        <td>{{$item->nama_permohonan_status}}</td>
                                        <td>{{$item->created_at}}</td>
                                        <th>

                                            <form action="/admin/permohonan-update" method="post" >
                                                {{csrf_field()}}
                                                <input type="hidden" name="p_id" value="{{$item->id}}">
                                                <input type="hidden" name="p_s_id" value="{{$item->permohonan_status_id}}">
                                                <table border="0">
                                                    <tr>
                                                        <td>
                                                            <select class="form-control custom-select" name="p_u_id" required>
                                                                <option value="{{$item->permohonan_status_id}}">{{$item->nama_permohonan_status}}</option>
                                                                @foreach($model['permohonan'] as $item)
                                                                <option value="{{$item->id}}">{{$item->nama}}</option>
                                                                @endforeach
                                                            </select>

                                                        </td>
                                                        <td>
                                                            <button type="submit" class="btn btn-info waves-effect waves-light m-r-10">Update</button>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </form>

                                        </th>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->
    @endsection