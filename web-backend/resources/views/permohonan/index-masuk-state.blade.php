@extends('index')
@section('content')
<!-- Bread crumb -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-primary">Dashboard</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->

    <!-- Container fluid  -->
    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-title">
                        <h4>Pilih Status Permohonan </h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <!-- <th>#</th> -->
                                        <!-- <th>Permohonan</th> -->
                                        <th>Nama</th>
                                        <!-- <th>Program Studi</th> -->
                                        <!-- <th>Created At</th> -->
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach($model as $item)
                                    <tr>
                                        <!-- <td>{{$item->id}}</td> -->
                                        <td>{{$item->nama}}</td>
                                        <!-- <td>{{$item->nama_user}}</td> -->
                                        <!-- <td>{{$item->prodi}}</td> -->
                                        <!-- <td>{{$item->created_at}}</td> -->
                                        <th>

                                            <form action="{{route('permohonan.destroy', $item->id)}}" method="post" id="form_delete" accept-charset="UTF-8">
                                                <input name="_method" type="hidden" value="DELETE">
                                                <input name="_token" type="hidden" value="{{ csrf_token() }}">
                                                <a href="/admin/permohonan-masuk-result/{{$item->id}}/{{$item->permohonan_id}}">
                                                    <button type="button" class="btn btn-info waves-effect waves-light m-r-10">Pilih</button>
                                                </a>
                                                <!-- <button type="submit" class="btn btn-danger waves-effect waves-light m-r-10">Delete</button> -->
                                            </form>

                                        </th>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->
    @endsection