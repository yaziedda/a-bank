@extends('index')
@section('content')
<!-- Bread crumb -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-primary">Dashboard</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->

    <!-- Container fluid  -->
    <div class="container-fluid">

        <p>
          <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
            Input Data
          </a>
        </p>

        <div class="row" >
            <div class="col-lg-12">
                <div class="collapse" id="collapseExample">
                  <div class="card card-body">
                        <h4 class="card-title">Input Pengumuman</h4>
                        <h6 class="card-subtitle">isi dengan data yang benar</h6>
                        <form action="{{route('pengumuman.store')}}" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="exampleInputuname">Judul</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="exampleInputuname" placeholder="Judul" name="judul" required="">
                                </div>
                            </div>
                            <div class="form-group">
                                <textarea class="textarea_editor form-control" name="content" rows="15" placeholder="Enter text ..." style="height:450px"></textarea>
                            </div>
                            <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                        </form>
                  </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-title">
                        <h4>Recent Orders </h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Judul</th>
                                        <!-- <th>Konten</th> -->
                                        <th>Created At</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach($model as $item)
                                    <tr>
                                        <td>{{$item->id}}</td>
                                        <td>{{$item->judul}}</td>
                                        <!-- <td>{{$item->content}}</td> -->
                                        <td>{{$item->created_at}}</td>
                                        <th>

                                            <form action="{{route('pengumuman.destroy', $item->id)}}" method="post" id="form_delete" accept-charset="UTF-8">
                                                <input name="_method" type="hidden" value="DELETE">
                                                <input name="_token" type="hidden" value="{{ csrf_token() }}">
                                                <a href="{{route('pengumuman.edit', $item->id)}}">
                                                    <button type="button" class="btn btn-info waves-effect waves-light m-r-10">Edit</button>
                                                </a>
                                                <button type="submit" class="btn btn-danger waves-effect waves-light m-r-10">Delete</button>
                                            </form>

                                        </th>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->
    @endsection