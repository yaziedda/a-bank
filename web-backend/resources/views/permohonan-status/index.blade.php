@extends('index')
@section('content')
<!-- Bread crumb -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-primary">Dashboard</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->

    <!-- Container fluid  -->
    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Input User</h4>
                        <h6 class="card-subtitle">isi dengan data yang benar</h6>
                        <form action="{{route('permohonan-status.store')}}" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="exampleInputuname">Permohonan</label>
                                <select class="form-control custom-select" required="" name="permohonan_id">
                                    <option>-- Pilih Permohonan--</option>
                                    @foreach($model['permohonan'] as $item)
                                    <option value="{{$item->id}}">{{$item->nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputuname">Nama</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="exampleInputuname" placeholder="Nama" name="nama" required="">
                                </div>
                            </div>

                            <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-title">
                        <h4>Recent Orders </h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Permohonan</th>
                                        <th>Status</th>
                                        <th>Created At</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach($model['data'] as $item)
                                    <tr>
                                        <td>{{$item->id}}</td>
                                        <td>{{$item->nama_permohonan}}</td>
                                        <td>{{$item->nama}}</td>
                                        <td>{{$item->created_at}}</td>
                                        <th>

                                            <form action="{{route('permohonan-status.destroy', $item->id)}}" method="post" id="form_delete" accept-charset="UTF-8">
                                                <input name="_method" type="hidden" value="DELETE">
                                                <input name="_token" type="hidden" value="{{ csrf_token() }}">
                                                <a href="{{route('permohonan-status.edit', $item->id)}}">
                                                    <button type="button" class="btn btn-info waves-effect waves-light m-r-10">Edit</button>
                                                </a>
                                                <button type="submit" class="btn btn-danger waves-effect waves-light m-r-10">Delete</button>
                                            </form>

                                        </th>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->
    @endsection