@extends('index')
@section('content')
<!-- Bread crumb -->
<script type="text/javascript" src="{{ URL::asset('js/qrcode.js')}}"></script>
<style type="text/css">
#qrcode {
  width:160px;
  height:160px;
  /*margin-top:15px;*/
  margin: 0;
  background: red;
}
</style>
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-primary">Dashboard</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->

    <!-- Container fluid  -->
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-4">
                <a href="/antrian">
                    <div class="card bg-primary p-20">
                        <div class="media widget-ten">
                            <div class="media-left meida media-middle">
                                <span><i class="ti-bag f-s-40"></i></span>
                            </div>
                            <div class="media-body media-text-right">
                                <h2 class="color-white">{{$model['service']->name}}</h2>
                                <p class="m-b-0">{{$model['service']->description}}</p>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        <!--     <div class="col-md-3">
                <div class="card bg-pink p-20">
                    <div class="media widget-ten">
                        <div class="media-left meida media-middle">
                            <span><i class="ti-comment f-s-40"></i></span>
                        </div>
                        <div class="media-body media-text-right">
                            <h2 class="color-white">278</h2>
                            <p class="m-b-0">New Comment</p>
                        </div>
                    </div>
                </div>
            </div> -->
            <!-- <div class="col-md-3">
                <div class="card bg-success p-20">
                    <div class="media widget-ten">
                        <div class="media-left meida media-middle">
                            <span><i class="ti-vector f-s-40"></i></span>
                        </div>
                        <div class="media-body media-text-right">
                            <h2 class="color-white">$27647</h2>
                            <p class="m-b-0">Bounce Rate</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card bg-danger p-20">
                    <div class="media widget-ten">
                        <div class="media-left meida media-middle">
                            <span><i class="ti-location-pin f-s-40"></i></span>
                        </div>
                        <div class="media-body media-text-right">
                            <h2 class="color-white">278</h2>
                            <p class="m-b-0">Total Visitor</p>
                        </div>
                    </div>
                </div>
            </div> -->
        </div>

        <div class="row">

            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <br><br><br>
                                <h1 class="text-center">
                                    {{$model['bank']->name}}<br>
                                    <div class="text-center" style="font-size: 14px;">{{$model['bank']->address}}</div>
                                    <div class="text-center" style="font-size: 12px; color: red;">* SCAN qr code ini untuk verifikasi antrian anda</div>
                                </h1>
                            </div>
                            <div class="col-md-6">
                                <div id="qrcode" style="width:100px; height:100px; margin-top:15px;"></div>

                            </div>                            
                        </div>
                        <br><br>
                        <div class="card-two">
                            <h3>A-BANK ( Antrian bank online )</h3>
                            <div class="desc">
                                2018
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->
    <script type="text/javascript">
        var qrcode = new QRCode(document.getElementById("qrcode"), {
            width : 250,
            height : 250,
            margin : 0
        });

        

        function makeCode () {      
            var bank = {!! $model['bank'] !!}
            var service = {!! $model['service'] !!}
            var qrId = +bank['id'] +'-'+service['id'];
            qrId = qrId+"-"+getFormattedDate();

            console.log(qrId);
            qrcode.makeCode(qrId);
        }

        function getFormattedDate() {
            var date = new Date();
            var str = date.getFullYear() + "" + (date.getMonth() + 1) + "" + date.getDate() + "" +  date.getHours() + "" + date.getMinutes() + "" + date.getSeconds();

            return str;
        }

        function refreshData() {
            x = 5;
            makeCode();
            setTimeout(refreshData, x*1000);
        }

        refreshData();
    </script>
    @endsection