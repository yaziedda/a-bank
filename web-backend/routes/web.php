<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'BankAppsCT@check');

Route::get('/login', 'BankAppsCT@loginUI');
Route::post('/login-user', 'BankAppsCT@login');

Route::group(['middleware' => 'checklogin'], function () {
	Route::get('/dashboard', 'BankAppsCT@index');
	Route::get('/antrian', 'BankAppsCT@bookingRequest');
	// Route::get('/antrian{$id}', 'BankAppsCT@bookingRequestDetail');
	Route::get('/service/{id}', 'BankAppsCT@bookingRequestDetail');
	Route::get('/service-skip/{id}', 'BankAppsCT@skipBooking');
	Route::get('/service-done/{id}', 'BankAppsCT@doneBooking');
	Route::get('/logout', 'BankAppsCT@logout');
});