<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/auth', 'MobileCT@auth');
Route::post('/reg', 'MobileCT@reg');
Route::get('/bank', 'MobileCT@getBank');
Route::get('/service', 'MobileCT@getService');
Route::post('/booking', 'MobileCT@booking');
Route::post('/booking-confirm', 'MobileCT@bookingConfirm');
Route::get('/my-booking', 'MobileCT@myBooking');
Route::get('/my-booking-detail', 'MobileCT@myBookingDetail');
Route::post('/verify', 'MobileCT@verify');
