var colorPrimary;
var colorPrimaryDark;
var colorPrimaryButton;

// var defaultColor = "#4CAF50";

window.addEventListener("load", startup, false);
function startup() {
	colorPrimary = document.querySelector("#colorPrimary");
	// colorPrimary.value = defaultColor;
	colorPrimary.addEventListener("input", updateColorPrimary, false);
	colorPrimary.select();

	colorPrimaryDark = document.querySelector("#colorPrimaryDark");
	// colorPrimaryDark.value = "#388E3C";
	colorPrimaryDark.addEventListener("input", updateColorPrimaryDark, false);
	colorPrimaryDark.select();

	colorPrimaryButton = document.querySelector("#colorPrimaryButton");
	// colorPrimaryButton.value = "#FFC107";
	colorPrimaryButton.addEventListener("input", updateColorButton, false);
	colorPrimaryButton.select();
}
function updateColorPrimary(event) {
	var p = document.querySelector("#color-primary-c1");

	if (p) {
		p.style.background = event.target.value;
	}

	p = document.querySelector("#color-primary-c2-p1");

	if (p) {
		p.style.background = event.target.value;
	}

	p = document.querySelector("#color-primary-c2-p2");

	if (p) {
		p.style.background = event.target.value;
	}

	p = document.querySelector("#color-primary-c3");

	if (p) {
		p.style.background = event.target.value;
	}}

function updateColorPrimaryDark(event) {
	

	var cutsom2p2 = document.querySelector("#color-dark-c1");

	if (cutsom2p2) {
		cutsom2p2.style.background = event.target.value;
	}

	var cutsom2p3 = document.querySelector("#color-dark-c3");

	if (cutsom2p3) {
		cutsom2p3.style.background = event.target.value;
	}


}

function updateColorButton(event) {
	

	var cutsom2p2 = document.querySelector("#color-button");

	if (cutsom2p2) {
		cutsom2p2.style.background = event.target.value;
	}


}

function updateAll(event) {
	document.querySelectorAll("p").forEach(function(p) {
		p.style.color = event.target.value;
	});
}
