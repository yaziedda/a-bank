<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceModel extends Model
{
    protected $table = "tm_service";
    protected $primaryKey = "id";
    public $timestamps = false;
}
