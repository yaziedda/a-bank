<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrxBookingModel extends Model
{
    protected $table = "tr_booking";
    protected $primaryKey = "id";
    public $timestamps = false;
}
