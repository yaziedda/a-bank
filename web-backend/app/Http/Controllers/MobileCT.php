<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserModel;
use App\BankModel;
use App\ServiceModel;
use App\TrxBookingModel;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class MobileCT extends Controller
{

    public function auth(Request $request){

        $user = new UserModel();
        $user = $user->where('phone', $request->phone)->first();
        if($user == null){
            // $user = new UserModel();
            // $user->name = $request->name;
            // $user->phone = $request->phone;
            // $user->email = $request->email;
            // $user->created_at = Carbon::now('Asia/Jakarta')->format('Y-m-d H:m:s');
            // $user->save();

            $model['state'] = false;
            $model['data'] = null;
        }else{
            $model['state'] = true;
            $model['data'] = $user;
        }
        return $model;
    }

    public function reg(Request $request){

        $user = new UserModel();
        $user = $user->where('phone', $request->phone)->first();
        if($user == null){
            $user = new UserModel();
            $user->name = $request->name;
            $user->phone = $request->phone;
            $user->email = $request->email;
            $user->created_at = Carbon::now('Asia/Jakarta')->format('Y-m-d H:m:s');
            $user->status = '1';
            $user->save();

            $model['state'] = true;
            $model['data'] = $user;
        }else{
            $model['state'] = true;
            $model['data'] = $user;
        }
        return $model;
    }

    public function getBank(Request $request){

        $data = new BankModel();
        $data = $data->where('status', '1')->get();

        $model['state'] = false;
        $model['data'] = $data;
        return $model;
    }

    public function getService(Request $request){

        $data = new ServiceModel();
        $data = $data->where('status', '1')->get();

        $model['state'] = false;
        $model['data'] = $data;
        return $model;
    }


    public function booking(Request $request){

        $user_id = $request->user_id;
        $bank_id = $request->bank_id;
        $service_id = $request->service_id;

        // $user_id = 1;
        // $bank_id = 1;
        // $service_id = 2;

        $dateNow = Carbon::now()->toDateString();

        $bookingCheck = new TrxBookingModel();
        $bookingCheck = $bookingCheck
                    // ->where(['bank_id' => $bank_id, 'service_id' => $service_id, 'user_id' => $user_id, 'date' => $dateNow])
                    ->where(['user_id' => $user_id, 'date' => $dateNow])
                    ->where('status', '!=', '1')
                    ->first();
        if($bookingCheck != null){
            $model['state'] = false;
            $model['data'] = 1;
            return $model;
        }

        $user = new UserModel();
        $user = $user->where('id', $user_id)->first();

        $bank = new BankModel();
        $bank = $bank->where('id', $bank_id)->first();

        $service = new ServiceModel();
        $service = $service->where('id', $service_id)->first();

        $booking = new TrxBookingModel();
        $booking = $booking
                    ->select(DB::raw('max(time) as time'))
                    ->where(['bank_id' => $bank_id, 'service_id' => $service_id, 'date' => $dateNow])->first();

        $firstTime = false;
        if($booking->time == null){
            $firstTime = true;
            $booking->time = Carbon::createFromFormat('H:i:s', $bank->start_time)->format('H:i:s');
        }

        $booking_time = $booking->time;
        
        $booking_time_with_estimate = Carbon::createFromFormat('H:i:s', $booking_time)->addMinutes($service->estimate+$service->estimate)->format('H:i:s');
        $breakTime = false;
        if($booking_time_with_estimate != $bank->start_break_time && $booking_time_with_estimate >= $bank->start_break_time && $booking_time_with_estimate <= $bank->end_break_time){
            $breakTime = true;
            $booking_time_with_estimate = $bank->end_break_time;
        }else{
            if($firstTime){
                $booking_time_with_estimate = Carbon::createFromFormat('H:i:s', $bank->start_time)->format('H:i:s');
            }else{
                $booking_time_with_estimate = Carbon::createFromFormat('H:i:s', $booking_time)->addMinutes($service->estimate)->format('H:i:s');
            }
        }

        if(Carbon::createFromFormat('H:i:s', $booking_time_with_estimate)->addMinutes($service->estimate)->format('H:i:s') > $bank->end_time){
            $model['state'] = false;
            $model['data'] = 2;
            return $model;
        }

        // if(Carbon::now()->format('H:i:s') > $bank->end_time){
        //     $model['state'] = false;
        //     $model['data'] = 3;
        //     return $model;
        // }

        // if(Carbon::createFromFormat('H:i:s', $booking_time_with_estimate)->format('H:i:s') <= $bank->start_time){
        //     $timeNow = Carbon::now()->format('H:i:s');
        //     // if($timeNow > $bank->end_time){
        //     //     $model['state'] = false;
        //     //     $model['data'] = 3;
        //     //     return $model;
        //     // }
        //     $booking_time_with_estimate = $timeNow;
        // }

        $tglIndo = Carbon::createFromFormat('H:i:s', $booking_time_with_estimate)->format('D');
        $bulan = Carbon::createFromFormat('H:i:s', $booking_time_with_estimate)->addMinutes($service->estimate)->format('d M Y');
        $booking_time_with_estimate = Carbon::createFromFormat('H:i:s', $booking_time_with_estimate)->format('H:i').' WIB';
        switch ($tglIndo) {
            case 'Sun':
                $tglIndo = 'Minggu, '.$bulan;
                break;
            case 'Mon':
                $tglIndo = 'Senin, '.$bulan;
                break;
            case 'Tue':
                $tglIndo = 'Selasa, '.$bulan;
                break;
            case 'Wed':
                $tglIndo = 'Rabu, '.$bulan;
                break;
            case 'Thu':
                $tglIndo = 'Kamis, '.$bulan;
                break;
            case 'Fri':
                $tglIndo = 'Jumat, '.$bulan;
                break;
            case 'Sat':
                $tglIndo = 'Sabtu, '.$bulan;
                break;
            
            default:
                $tglIndo = 'Sabtu, 1 Jul 2018';
                break;
        }

        $data['user'] = $user;
        $data['bank'] = $bank;
        $data['service'] = $service;
        $data['date'] = $tglIndo;
        $data['time'] = $booking_time_with_estimate;

        $model['state'] = true;
        $model['data'] = $data;
        return $model;
    }

    public function bookingConfirm(Request $request){

        $user_id = $request->user_id;
        $bank_id = $request->bank_id;
        $service_id = $request->service_id;

        // $user_id = 2;
        // $bank_id = 1;
        // $service_id = 2;

        $dateNow = Carbon::now()->toDateString();

        $user = new UserModel();
        $user = $user->where('id', $user_id)->first();

        $bank = new BankModel();
        $bank = $bank->where('id', $bank_id)->first();

        $service = new ServiceModel();
        $service = $service->where('id', $service_id)->first();

        $booking = new TrxBookingModel();
        $booking = $booking
                    ->select(DB::raw('max(time) as time'))
                    ->where(['bank_id' => $bank_id, 'service_id' => $service_id, 'date' => $dateNow])->first();

        $firstTime = false;
        if($booking->time == null){
            $firstTime = true;
            $booking->time = Carbon::createFromFormat('H:i:s', $bank->start_time)->format('H:i:s');
        }

        $booking_time = $booking->time;
        
        $booking_time_with_estimate = Carbon::createFromFormat('H:i:s', $booking_time)->addMinutes($service->estimate+$service->estimate)->format('H:i:s');

        $breakTime = false;
        if($booking_time_with_estimate != $bank->start_break_time && $booking_time_with_estimate >= $bank->start_break_time && $booking_time_with_estimate <= $bank->end_break_time){
            $breakTime = true;
            $booking_time_with_estimate = $bank->end_break_time;
        }else{
            if($firstTime){
                $booking_time_with_estimate = Carbon::createFromFormat('H:i:s', $bank->start_time)->format('H:i:s');
            }else{
                $booking_time_with_estimate = Carbon::createFromFormat('H:i:s', $booking_time)->addMinutes($service->estimate)->format('H:i:s');
            }
        }

        if(Carbon::createFromFormat('H:i:s', $booking_time_with_estimate)->addMinutes($service->estimate)->format('H:i:s') > $bank->end_time){
            $model['state'] = false;
            $model['data'] = null;
            return $model;
        }


        // if(Carbon::createFromFormat('H:i:s', $booking_time_with_estimate)->format('H:i:s') <= $bank->start_time){
        //     $timeNow = Carbon::now()->format('H:i:s');
        //     if($timeNow > $bank->end_time){
        //         $model['state'] = false;
        //         $model['data'] = 3;
        //         return $model;
        //     }
        //     $booking_time_with_estimate = $timeNow;
        // }

        $bookingCountToday = DB::table('tr_booking')->select(DB::raw('count(*) as count'))->where(['bank_id' => $bank_id, 'service_id' => $service_id, 'date' => $dateNow])->first();

        $code_booking = $service->code.str_pad($bookingCountToday->count+1, 3, '0', STR_PAD_LEFT);;

        $barcode_id = $user_id.$bank_id.$service_id.$code_booking.Carbon::now()->timestamp;

        $bookingSave = new TrxBookingModel();
        $bookingSave->user_id = $user_id;
        $bookingSave->bank_id = $bank_id;
        $bookingSave->service_id = $service_id;
        $bookingSave->code_booking = $code_booking;
        $bookingSave->barcode_id = $barcode_id;
        $bookingSave->time = Carbon::createFromFormat('H:i:s', $booking_time_with_estimate)->format('H:i');
        $bookingSave->date = $dateNow;
        $bookingSave->save();

        $tglIndo = Carbon::createFromFormat('H:i:s', $booking_time_with_estimate)->format('D');
        $bulan = Carbon::createFromFormat('H:i:s', $booking_time_with_estimate)->addMinutes($service->estimate)->format('d M Y');
        $booking_time_with_estimate = $booking_time_with_estimate.' WIB';
        switch ($tglIndo) {
            case 'Sun':
                $tglIndo = 'Minggu, '.$bulan;
                break;
            case 'Mon':
                $tglIndo = 'Senin, '.$bulan;
                break;
            case 'Tue':
                $tglIndo = 'Selasa, '.$bulan;
                break;
            case 'Wed':
                $tglIndo = 'Rabu, '.$bulan;
                break;
            case 'Thu':
                $tglIndo = 'Kamis, '.$bulan;
                break;
            case 'Fri':
                $tglIndo = 'Jumat, '.$bulan;
                break;
            case 'Sat':
                $tglIndo = 'Sabtu, '.$bulan;
                break;
            
            default:
                $tglIndo = 'Sabtu, 1 Jul 2018';
                break;
        }

        $data['user'] = $user;
        $data['bank'] = $bank;
        $data['service'] = $service;
        $data['date'] = $tglIndo;
        $data['time'] = $booking_time_with_estimate;

        $model['state'] = true;
        $model['data'] = $data;
        return $model;
    }

    public function myBooking(Request $request){
        $user_id = $request->user_id;
        // $user_id = 1;

        $dateNow = Carbon::now()->toDateString();

        $trxBooking = DB::table('tr_booking')
                        ->select('tr_booking.*', 'tr_booking.date as date_id', 'tm_bank.name as bank_name', 'tm_bank.address as bank_address', 'tm_service.name as service_name', 'tm_service.description as service_description', DB::raw('TIME_FORMAT(time, "%H:%i") as time'), DB::raw('DATE_FORMAT(date, "%d %M %Y") as date'), DB::raw('TIME_FORMAT(TIMEDIFF(time, curtime()), "%H` j %i` min") as diff'))
                        ->join('tm_bank', 'tr_booking.bank_id', '=', 'tm_bank.id')
                        ->join('tm_service', 'tr_booking.service_id', '=', 'tm_service.id')
                        ->where('user_id', $user_id)
                        ->get();

        $model['state'] = true;
        $model['data'] = $trxBooking;
        return $model;
    }

    public function myBookingDetail(Request $request){
        $user_id = $request->user_id;
        $bank_id = $request->bank_id;
        $service_id = $request->service_id;
        $date = $request->date;

        // return $date;

        // $user_id = 1;
        // $bank_id = 1;
        // $service_id = 2;

        // $dateNow = Carbon::now()->toDateString();

        $trxBooking = DB::table('tr_booking')
                        ->select('tr_booking.*', 'tm_bank.name as bank_name', 'tm_bank.address as bank_address', 'tm_service.name as service_name', 'tm_service.description as service_description', DB::raw('TIME_FORMAT(time, "%H:%i") as time'), DB::raw('DATE_FORMAT(date, "%d %M %Y") as date'), DB::raw('TIME_FORMAT(TIMEDIFF(time, curtime()), "%H` j %i` min") as diff'))
                        ->join('tm_bank', 'tr_booking.bank_id', '=', 'tm_bank.id')
                        ->join('tm_service', 'tr_booking.service_id', '=', 'tm_service.id')
                        ->where(['bank_id' => $bank_id, 'service_id' => $service_id, 'date' => $date])
                        ->get();

        $model['state'] = true;
        $model['data'] = $trxBooking;
        return $model;
    }

    public function verify(Request $request){

        $user_id = $request->user_id;
        $tr_id = $request->tr_id;
        $qr_id = $request->qr_id;
        
        $trxBooking = new TrxBookingModel();
        $trxBooking = $trxBooking->where(['id' => $tr_id, 'user_id' => $user_id])->first();

        if($trxBooking != null){
            $trxBooking->arrived = '1';
            $trxBooking->arrived_code = $qr_id;
            $trxBooking->save();

            $model['state'] = true;
            $model['data'] = $trxBooking;
        }else{
            $model['state'] = false;
            $model['data'] = null;
        }

        
        return $model;
    }

    
}
