package id.abank.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.abank.R;
import id.abank.model.MyTrxBooking;

public class MyBookingAdapter extends RecyclerView.Adapter<MyBookingAdapter.ViewHolder> {

    List<MyTrxBooking> list = new ArrayList<>();


    public interface OnItemClickListener {
        void onItemClick(MyTrxBooking model);
    }

    private final OnItemClickListener listener;

    public MyBookingAdapter(List<MyTrxBooking> list, OnItemClickListener listener) {
        this.list = list;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_my_booking, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final MyTrxBooking model = list.get(position);

        if(model.getArrived() == 0){
            holder.rlNotV.setVisibility(View.VISIBLE);
            holder.rlScanNotV.setVisibility(View.VISIBLE);
            holder.llV.setVisibility(View.GONE);
        }else{
            holder.rlNotV.setVisibility(View.GONE);
            holder.rlScanNotV.setVisibility(View.GONE);
            holder.llV.setVisibility(View.VISIBLE);
        }

        holder.tvBank.setText(model.getBankName());
        holder.tvService.setText(model.getServiceName());
        holder.tvCode.setText(model.getCodeBooking());
        holder.tvDate.setText(model.getDate());
        holder.tvEstimate.setText(model.getDiff().replaceAll("`", "'"));
        holder.tvTime.setText(model.getTime());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(model);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.ll_v)
        LinearLayout llV;
        @BindView(R.id.rl_not_v)
        LinearLayout rlNotV;
        @BindView(R.id.tv_bank)
        TextView tvBank;
        @BindView(R.id.tv_date)
        TextView tvDate;
        @BindView(R.id.tv_service)
        TextView tvService;
        @BindView(R.id.tv_estimate)
        TextView tvEstimate;
        @BindView(R.id.tv_code)
        TextView tvCode;
        @BindView(R.id.tv_time)
        TextView tvTime;
        @BindView(R.id.iv_scan)
        ImageView ivScan;
        @BindView(R.id.rl_scan_not_v)
        RelativeLayout rlScanNotV;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
