package id.abank.client;

import android.content.Context;

import id.abank.client.mobile.MobileService;
import id.abank.util.Static;


public class ApiUtils {

    public static String API = Static.BASE_URL;

    public static MobileService MobileService(Context context){
        return RetrofitClient.getClient(context, API).create(MobileService.class);
    }


}
