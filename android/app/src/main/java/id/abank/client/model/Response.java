package id.abank.client.model;

/**
 * Created by Dizzay on 11/10/2017.
 */

public class Response {

    boolean state;
    Object data;

    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
