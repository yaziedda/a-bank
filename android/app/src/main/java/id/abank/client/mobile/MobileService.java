package id.abank.client.mobile;


import java.util.Map;

import id.abank.client.model.Response;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

/**
 * Created by Dizzay on 11/10/2017.
 */

public interface MobileService {

    @FormUrlEncoded
    @POST("auth")
    Call<Response> auth(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST("reg")
    Call<Response> reg(@FieldMap Map<String, String> map);

    @GET("bank")
    Call<Response> getBank();

    @GET("service")
    Call<Response> getService();

    @FormUrlEncoded
    @POST("booking")
    Call<Response> booking(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST("booking-confirm")
    Call<Response> bookingConfirm(@FieldMap Map<String, String> map);

    @GET("my-booking")
    Call<Response> getMyBooking(@QueryMap Map<String, String> map);

    @GET("my-booking-detail")
    Call<Response> getMyBookingDetail(@QueryMap Map<String, String> map);

    @FormUrlEncoded
    @POST("verify")
    Call<Response> bookingVerify(@FieldMap Map<String, String> map);

    @GET
    public Call<ResponseBody> kursUSD_IDR(@Url String url);

}
