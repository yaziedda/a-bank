package id.abank.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.abank.R;
import id.abank.client.ApiUtils;
import id.abank.client.mobile.MobileService;
import id.abank.client.model.Response;
import id.abank.model.User;
import id.abank.util.BaseActivity;
import id.abank.util.Preferences;
import id.abank.util.Static;
import retrofit2.Call;
import retrofit2.Callback;

public class RegisterActivity extends BaseActivity {

    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.et_phone)
    TextInputEditText etPhone;
    @BindView(R.id.et_name)
    TextInputEditText etName;
    @BindView(R.id.et_mail)
    TextInputEditText etMail;
    String phone;
    MobileService mobileService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        phone = getIntent().getStringExtra("phone");
        mobileService = ApiUtils.MobileService(getApplicationContext());

        etPhone.setText(phone);
        etPhone.setEnabled(false);

    }

    @OnClick({R.id.iv_finish, R.id.bt_cancel, R.id.bt_confirm})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_finish:
                finish();
                break;
            case R.id.bt_cancel:
                finish();
                break;
            case R.id.bt_confirm:
                String name = etName.getText().toString();
                String mai = etMail.getText().toString();
                if(name.isEmpty() && mai.isEmpty()){
                    showMessage("Belum lengkap !");
                    return;
                }

                showPleasewaitDialog();

                Map<String, String> map = new HashMap<>();
                map.put("phone", phone);
                map.put("name", name);
                map.put("email", mai);

                mobileService.reg(map).enqueue(new Callback<Response>() {
                    @Override
                    public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                        dissmissPleasewaitDialog();
                        if(response.isSuccessful()){
                            Response body = response.body();
                            if(body.isState()){
                                User user = new Gson().fromJson(new Gson().toJson(body.getData()), User.class);
                                showMessage("Welcome to A-Bank, "+user.getName());
                                Preferences.setUser(getApplicationContext(), user);
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            }else{
                                showMessage(Static.SOMETHING_WRONG);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<Response> call, Throwable t) {
                        dissmissPleasewaitDialog();
                        showMessage(Static.SOMETHING_WRONG);
                    }
                });
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
}
