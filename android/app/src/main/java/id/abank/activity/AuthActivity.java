package id.abank.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.AccountKitLoginResult;
import com.facebook.accountkit.PhoneNumber;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.abank.R;
import id.abank.client.ApiUtils;
import id.abank.client.mobile.MobileService;
import id.abank.client.model.Response;
import id.abank.model.TrxBooking;
import id.abank.model.User;
import id.abank.util.BaseActivity;
import id.abank.util.Preferences;
import retrofit2.Call;
import retrofit2.Callback;

import static id.abank.util.Static.MyPref;

public class AuthActivity extends BaseActivity {

    @BindView(R.id.tv_auth)
    TextView tvAuth;
    MobileService mobileService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);
        ButterKnife.bind(this);
        tvAuth.setVisibility(View.GONE);

        mobileService = ApiUtils.MobileService(getApplicationContext());

//        if(Preferences.getUser(getApplicationContext()) == null){
//            User user = new User();
//            user.setId(1);
//            user.setName("Yazied DA");
//            user.setEmail("yazieddabr@gmail.com");
//            user.setPhone("+6289662549895");
//            Preferences.setUser(getApplicationContext(), user);
//        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }finally {
                    if(Preferences.getUser(getApplicationContext()) != null){
                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                        finish();
                    }else{
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                tvAuth.setVisibility(View.VISIBLE);
                                tvAuth.setAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.push_out));
                            }
                        });
                    }
                }
            }
        }).start();

        tvAuth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                phoneLogin();
            }
        });
    }

    @Override
    protected void onActivityResult(
            final int requestCode,
            final int resultCode,
            final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == APP_REQUEST_CODE) { // confirm that this response matches your request
            AccountKitLoginResult loginResult = data.getParcelableExtra(AccountKitLoginResult.RESULT_KEY);
            String toastMessage;
            if (loginResult.getError() != null) {
                toastMessage = loginResult.getError().getErrorType().getMessage();
                Toast.makeText(getApplicationContext(), loginResult.getError().getUserFacingMessage(), Toast.LENGTH_LONG).show();
            } else if (loginResult.wasCancelled()) {
                toastMessage = "Login Cancelled";
            } else {
                if (loginResult.getAccessToken() != null) {
                    toastMessage = "Success:" + loginResult.getAccessToken().getAccountId();
                } else {
                    toastMessage = String.format(
                            "Success:%s...",
                            loginResult.getAuthorizationCode().substring(0,10));
                }
                AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
                    @Override
                    public void onSuccess(final Account account) {
                        final PhoneNumber number = account.getPhoneNumber();
                        if (number != null) {
                            final String phoneNumber = number.toString();//.replaceFirst("\\+62", "0");
                            Map<String, String> params  = new HashMap<String, String>();
                            params.put("phone", phoneNumber);
                            showPleasewaitDialog();
                            mobileService.auth(params).enqueue(new Callback<Response>() {
                                @Override
                                public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                                    dissmissPleasewaitDialog();
                                    Response body = response.body();
                                    User user = new Gson().fromJson(new Gson().toJson(body.getData()), User.class);
                                    if(!body.isState()){
                                        showMessage("Hello first time to use A-Bank");
                                        Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
                                        intent.putExtra("phone", phoneNumber);
                                        startActivity(intent);
//                                        finish();
                                    }else{
                                        showMessage("Welcome back to A-Bank, "+user.getName());
                                        Preferences.setUser(getApplicationContext(), user);
                                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                                        finish();
                                    }

                                }

                                @Override
                                public void onFailure(Call<Response> call, Throwable t) {
                                    dissmissPleasewaitDialog();
                                }
                            });

                        }else{
                            Log.e("error", "Number null=");
                        }
                    }

                    @Override
                    public void onError(final AccountKitError error) {
                        Log.e("error", "AccountKitError=" + error);
                    }
                });
            }


        }
    }

    public static int APP_REQUEST_CODE = 99;
//
//    public void phoneLogin() {
//        final Intent intent = new Intent(AuthActivity.this, AccountKitActivity.class);
//        AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder =
//                new AccountKitConfiguration.AccountKitConfigurationBuilder(
//                        LoginType.PHONE,
//                        AccountKitActivity.ResponseType.TOKEN);
//
//        intent.putExtra(
//                AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
//                configurationBuilder.build());
//        startActivityForResult(intent, APP_REQUEST_CODE);
//    }


    public void phoneLogin() {
        final Intent intent = new Intent(AuthActivity.this, AccountKitActivity.class);
        AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder =
                new AccountKitConfiguration.AccountKitConfigurationBuilder(
                        LoginType.PHONE,
                        AccountKitActivity.ResponseType.TOKEN); // or .ResponseType.TOKEN
        // ... perform additional configuration ...
        intent.putExtra(
                AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                configurationBuilder.build());
        startActivityForResult(intent, APP_REQUEST_CODE);
    }
}
