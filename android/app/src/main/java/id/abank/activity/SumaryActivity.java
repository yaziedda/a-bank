package id.abank.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.abank.R;
import id.abank.adapter.MyBookingDetailAdapter;
import id.abank.client.ApiUtils;
import id.abank.client.mobile.MobileService;
import id.abank.client.model.Response;
import id.abank.model.Bank;
import id.abank.model.MyTrxBooking;
import id.abank.model.Service;
import id.abank.model.TrxBooking;
import id.abank.model.User;
import id.abank.util.BaseActivity;
import id.abank.util.Preferences;
import id.abank.util.Static;
import retrofit2.Call;
import retrofit2.Callback;

public class SumaryActivity extends BaseActivity {

    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_bank)
    TextView tvBank;
    @BindView(R.id.tv_service)
    TextView tvService;
    @BindView(R.id.tv_day)
    TextView tvDay;
    @BindView(R.id.tv_time)
    TextView tvTime;
    MobileService mobileService;
    Bank bank;
    Service service;
    User user;
    @BindView(R.id.bt_cancel)
    TextView btCancel;
    @BindView(R.id.bt_confirm)
    TextView btConfirm;
    @BindView(R.id.recycleview)
    RecyclerView recyclerView;
    @BindView(R.id.container)
    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sumary);
        ButterKnife.bind(this);

        swipeRefreshLayout.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorAccent,
                R.color.colorPrimaryDark);

        bank = (Bank) getIntent().getSerializableExtra("bank");
        service = (Service) getIntent().getSerializableExtra("service");
        user = Preferences.getUser(getApplicationContext());

        tvName.setText("");
        tvBank.setText("");
        tvService.setText("");
        tvDay.setText("");
        tvTime.setText("");

        mobileService = ApiUtils.MobileService(getApplicationContext());
        recyclerView.setNestedScrollingEnabled(false);
        btCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        loadData();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadData();
            }
        });

    }

    private void loadData() {
        final Map<String, String> map = new HashMap<>();
        map.put("user_id", String.valueOf(user.getId()));
        map.put("bank_id", String.valueOf(bank.getId()));
        map.put("service_id", String.valueOf(service.getId()));

//        showPleasewaitDialog();
        swipeRefreshLayout.setRefreshing(true);
        mobileService.booking(map).enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                if (response.isSuccessful()) {
                    Response body = response.body();
                    if (body.isState() && body.getData() != null) {
                        TrxBooking trxBooking = new Gson().fromJson(new Gson().toJson(body.getData()), TrxBooking.class);
                        tvName.setText(user.getName());
                        tvBank.setText(trxBooking.getBank().getName());
                        tvService.setText(trxBooking.getService().getName());
                        tvDay.setText(trxBooking.getDate());
                        tvTime.setText(trxBooking.getTime());

                        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                        Date today = Calendar.getInstance().getTime();
                        String date = df.format(today);

                        map.put("date", date);

                        mobileService.getMyBookingDetail(map).enqueue(new Callback<Response>() {
                            @Override
                            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
//                                dissmissPleasewaitDialog();
                                swipeRefreshLayout.setRefreshing(false);
                                if (response.isSuccessful()) {
                                    Response body = response.body();
                                    if (body.getData() != null) {
                                        Gson gson = new Gson();
                                        JsonObject jsonObject = gson.toJsonTree(body).getAsJsonObject();
                                        List<MyTrxBooking> listBody = gson.fromJson(jsonObject.getAsJsonArray("data"), new TypeToken<List<MyTrxBooking>>() {
                                        }.getType());
                                        MyBookingDetailAdapter adapter = new MyBookingDetailAdapter(listBody, new MyBookingDetailAdapter.OnItemClickListener() {
                                            @Override
                                            public void onItemClick(MyTrxBooking model) {
                                            }
                                        });
                                        final LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                                        recyclerView.setLayoutManager(layoutManager);
                                        recyclerView.setAdapter(adapter);
                                    }
                                } else {
                                    showMessage(Static.SOMETHING_WRONG);
                                }
                            }

                            @Override
                            public void onFailure(Call<Response> call, Throwable t) {
//                                dissmissPleasewaitDialog();
                                swipeRefreshLayout.setRefreshing(false);
                                showMessage(Static.SOMETHING_WRONG);
                                t.printStackTrace();
                            }
                        });

                        btConfirm.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                showPleasewaitDialog();
                                mobileService.bookingConfirm(map).enqueue(new Callback<Response>() {
                                    @Override
                                    public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                                        dissmissPleasewaitDialog();
                                        if (response.isSuccessful()) {
                                            Response body = response.body();
                                            if (body.isState()) {
                                                showMessage("Sukses Boking Antrian");
                                                PilihBankActivity.fa.finish();
                                                PertanyaanActivity.fa.finish();
                                                finish();
                                                startActivity(new Intent(getApplicationContext(), MyBookingActivity.class));
                                            } else {
                                                showMessage(Static.SOMETHING_WRONG);
                                                finish();
                                            }
                                        } else {
                                            showMessage(Static.SOMETHING_WRONG);
                                            finish();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<Response> call, Throwable t) {
                                        dissmissPleasewaitDialog();
                                        showMessage(Static.SOMETHING_WRONG);
                                        finish();
                                    }
                                });
                            }
                        });
                    } else {
                        Double code;
                        try {
                            code = (Double) body.getData();
                        } catch (Exception e) {
                            code = 0.0;
                        }

                        switch (code.intValue()) {
                            case 1:
                                showMessage("Anda masih memiliki antrian");
                                PilihBankActivity.fa.finish();
                                PertanyaanActivity.fa.finish();
                                finish();
                                startActivity(new Intent(getApplicationContext(), MyBookingActivity.class));
                                break;
                            case 2:
                                showMessage("Maaf, antrian penuh");
                                break;
                            case 3:
                                showMessage("Maaf, layanan telah tutup");
                                break;
                            default:
                                showMessage("Maaf, antrian tidak dapat di akses");
                                break;
                        }
                        finish();
                    }
                } else {
                    showMessage(Static.SOMETHING_WRONG);
                    finish();
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
//                dissmissPleasewaitDialog();
                swipeRefreshLayout.setRefreshing(false);
                showMessage(Static.SOMETHING_WRONG);
                finish();
            }
        });
    }
}
