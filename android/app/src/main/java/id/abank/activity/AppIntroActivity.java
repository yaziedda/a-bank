package id.abank.activity;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;

import com.github.paolorotolo.appintro.AppIntro2;
import com.github.paolorotolo.appintro.AppIntro2Fragment;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import id.abank.R;
import id.abank.util.Preferences;

public class AppIntroActivity extends AppIntro2 {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_app_intro);
        if(Preferences.getUser(getApplicationContext()) != null){
            startActivity(new Intent(getApplicationContext(), AuthActivity.class));
            finish();
        }
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "id.abank",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("Your Tag HASH", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        addSlide(AppIntro2Fragment.newInstance("Males Antri di BANK?", "Sekarang Gak Perlu Repot, ada A-BANK. Antrian BANK ONLINE secara online!", R.drawable.ic_abank_circle, ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary)));
        addSlide(AppIntro2Fragment.newInstance("A-BANK Solusi Antrian BANK", "Pakai A-BANK sekarang jadi mudah dan cepat ngantri di BANK !", R.drawable.ic_city_circle, ContextCompat.getColor(getApplicationContext(), R.color.colorAccent)));

        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), AuthActivity.class));
                finish();
            }
        });
    }
}
