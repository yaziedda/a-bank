package id.abank.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.abank.R;
import id.abank.adapter.MyBookingAdapter;
import id.abank.client.ApiUtils;
import id.abank.client.mobile.MobileService;
import id.abank.client.model.Response;
import id.abank.model.MyTrxBooking;
import id.abank.model.User;
import id.abank.util.BaseActivity;
import id.abank.util.Preferences;
import id.abank.util.Static;
import retrofit2.Call;
import retrofit2.Callback;

public class MyBookingActivity extends BaseActivity {

    MobileService mobileService;
    User user;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.container)
    SwipeRefreshLayout swipeRefreshLayout;
    private SwipeRefreshLayout mSwipeRefreshLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_booking);
        ButterKnife.bind(this);

        swipeRefreshLayout.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorAccent,
                R.color.colorPrimaryDark);

        mobileService = ApiUtils.MobileService(getApplicationContext());
        user = Preferences.getUser(getApplicationContext());

        loadData();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadData();
            }
        });

    }

    private void loadData() {
//        showPleasewaitDialog();
        swipeRefreshLayout.setRefreshing(true);
        Map<String, String> map = new HashMap<>();
        map.put("user_id", String.valueOf(user.getId()));

        mobileService.getMyBooking(map).enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
//                dissmissPleasewaitDialog();
                swipeRefreshLayout.setRefreshing(false);
                if (response.isSuccessful()) {
                    Response body = response.body();
                    if (body.getData() != null) {
                        Gson gson = new Gson();
                        JsonObject jsonObject = gson.toJsonTree(body).getAsJsonObject();
                        List<MyTrxBooking> listBody = gson.fromJson(jsonObject.getAsJsonArray("data"), new TypeToken<List<MyTrxBooking>>() {
                        }.getType());
                        MyBookingAdapter adapter = new MyBookingAdapter(listBody, new MyBookingAdapter.OnItemClickListener() {
                            @Override
                            public void onItemClick(MyTrxBooking model) {
                                Intent intent = new Intent(getApplicationContext(), MyBookingDetailActivity.class);
                                intent.putExtra("myTrxBooking", model);
                                startActivity(intent);
                                finish();
                            }
                        });
                        final LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                        recyclerView.setLayoutManager(layoutManager);
                        recyclerView.setAdapter(adapter);
                    }
                } else {
                    showMessage(Static.SOMETHING_WRONG);
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
//                dissmissPleasewaitDialog();
                swipeRefreshLayout.setRefreshing(false);
                showMessage(Static.SOMETHING_WRONG);
                t.printStackTrace();
            }
        });
    }

    @OnClick(R.id.iv_finish)
    public void onViewClicked() {
        finish();
    }
}
