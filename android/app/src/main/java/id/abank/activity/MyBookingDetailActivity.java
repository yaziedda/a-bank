package id.abank.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.abank.R;
import id.abank.adapter.MyBookingDetailAdapter;
import id.abank.client.ApiUtils;
import id.abank.client.mobile.MobileService;
import id.abank.client.model.Response;
import id.abank.model.MyTrxBooking;
import id.abank.util.BaseActivity;
import id.abank.util.Static;
import retrofit2.Call;
import retrofit2.Callback;

public class MyBookingDetailActivity extends BaseActivity {

    MyTrxBooking myTrxBooking;
    MobileService mobileService;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.ll_v)
    LinearLayout llV;
    @BindView(R.id.rl_not_v)
    LinearLayout rlNotV;
    @BindView(R.id.tv_bank)
    TextView tvBank;
    @BindView(R.id.tv_date)
    TextView tvDate;
    @BindView(R.id.tv_service)
    TextView tvService;
    @BindView(R.id.tv_estimate)
    TextView tvEstimate;
    @BindView(R.id.tv_code)
    TextView tvCode;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.iv_scan)
    ImageView ivScan;
    @BindView(R.id.rl_scan_not_v)
    RelativeLayout rlScanNotV;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.container)
    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_booking_detail);
        ButterKnife.bind(this);

        swipeRefreshLayout.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorAccent,
                R.color.colorPrimaryDark);

        myTrxBooking = (MyTrxBooking) getIntent().getSerializableExtra("myTrxBooking");
        mobileService = ApiUtils.MobileService(getApplicationContext());

        checkVerify();

        tvBank.setText(myTrxBooking.getBankName());
        tvService.setText(myTrxBooking.getServiceName());
        tvCode.setText(myTrxBooking.getCodeBooking());
        tvDate.setText(myTrxBooking.getDate());
        tvEstimate.setText(myTrxBooking.getDiff().replaceAll("`", "'"));
        tvTime.setText(myTrxBooking.getTime());

        loadData();

        ivScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TedPermission.with(MyBookingDetailActivity.this)
                        .setPermissionListener(new PermissionListener() {
                            @Override
                            public void onPermissionGranted() {
                                Intent i = new Intent(getApplicationContext(), ScanActivity.class);
                                i.putExtra("myTrxBooking", myTrxBooking);
                                startActivityForResult(i, 1812);
                            }

                            @Override
                            public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                                showMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]");
                            }
                        })
                        .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                        .setPermissions(Manifest.permission.CAMERA)
                        .check();

            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadData();
            }
        });


    }

    private void loadData() {
//        showPleasewaitDialog();
        swipeRefreshLayout.setRefreshing(true);

        Map<String, String> map = new HashMap<>();
        map.put("user_id", String.valueOf(myTrxBooking.getUserId()));
        map.put("bank_id", String.valueOf(myTrxBooking.getBankId()));
        map.put("service_id", String.valueOf(myTrxBooking.getServiceId()));
        map.put("date", myTrxBooking.getDateId());

        mobileService.getMyBookingDetail(map).enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
//                dissmissPleasewaitDialog();
                swipeRefreshLayout.setRefreshing(false);
                if (response.isSuccessful()) {
                    Response body = response.body();
                    if (body.getData() != null) {
                        Gson gson = new Gson();
                        JsonObject jsonObject = gson.toJsonTree(body).getAsJsonObject();
                        List<MyTrxBooking> listBody = gson.fromJson(jsonObject.getAsJsonArray("data"), new TypeToken<List<MyTrxBooking>>() {
                        }.getType());
                        MyBookingDetailAdapter adapter = new MyBookingDetailAdapter(listBody, new MyBookingDetailAdapter.OnItemClickListener() {
                            @Override
                            public void onItemClick(MyTrxBooking model) {

                            }
                        });
                        final LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                        recyclerView.setLayoutManager(layoutManager);
                        recyclerView.setAdapter(adapter);
                    }
                } else {
                    showMessage(Static.SOMETHING_WRONG);
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
//                dissmissPleasewaitDialog();
                swipeRefreshLayout.setRefreshing(false);
                showMessage(Static.SOMETHING_WRONG);
                t.printStackTrace();
            }
        });
    }

    private void checkVerify() {
        if (myTrxBooking.getArrived() == 0) {
            rlNotV.setVisibility(View.VISIBLE);
            rlScanNotV.setVisibility(View.VISIBLE);
            llV.setVisibility(View.GONE);
        } else {
            rlNotV.setVisibility(View.GONE);
            rlScanNotV.setVisibility(View.GONE);
            llV.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1812) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra("result");
                Map<String, String> map = new HashMap<>();
                map.put("user_id", String.valueOf(myTrxBooking.getUserId()));
                map.put("bank_id", String.valueOf(myTrxBooking.getBankId()));
                map.put("service_id", String.valueOf(myTrxBooking.getServiceId()));
                map.put("date", myTrxBooking.getDateId());
                map.put("tr_id", String.valueOf(myTrxBooking.getId()));
                map.put("qr_id", result);
                showPleasewaitDialog();
                mobileService.bookingVerify(map).enqueue(new Callback<Response>() {
                    @Override
                    public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                        dissmissPleasewaitDialog();
                        if (response.isSuccessful()) {
                            if (response.body().isState()) {
                                MyTrxBooking model = new Gson().fromJson(new Gson().toJson(response.body().getData()), MyTrxBooking.class);
                                myTrxBooking.setArrived(model.getArrived());
                                checkVerify();
                                showMessage("Sukses verifikasi");
                            } else {
                                showMessage("Gagal verifikasi");
                            }
                        } else {
                            showMessage(Static.SOMETHING_WRONG);
                        }
                    }

                    @Override
                    public void onFailure(Call<Response> call, Throwable t) {
                        dissmissPleasewaitDialog();
                        showMessage(Static.SOMETHING_WRONG);
                    }
                });
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
//                showMessage("Barcode tidak valid");
            }
        }
    }//onActivityResult

    private boolean checkQrFormat(String text) {
        try {
            String[] data = text.split("-");
            if (Integer.valueOf(data[0]) != myTrxBooking.getBankId()) {
                return false;
            }
            if (Integer.valueOf(data[1]) != myTrxBooking.getServiceId()) {
                return false;
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @OnClick(R.id.iv_finish)
    public void onViewClicked() {
        finish();
    }
}
