package id.abank.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.abank.R;
import id.abank.adapter.ServiceAdapter;
import id.abank.client.ApiUtils;
import id.abank.client.mobile.MobileService;
import id.abank.client.model.Response;
import id.abank.model.Bank;
import id.abank.model.Service;
import id.abank.util.BaseActivity;
import id.abank.util.Static;
import retrofit2.Call;
import retrofit2.Callback;

public class PertanyaanActivity extends BaseActivity {

    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_1)
    TextView tv1;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    MobileService mobileService;
    Bank bank;
    public static Activity fa;
    @BindView(R.id.container)
    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pertanyaan);
        ButterKnife.bind(this);
        tvTitle.setText("KEPERLUAN");

        swipeRefreshLayout.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorAccent,
                R.color.colorPrimaryDark);

        fa = this;

        bank = (Bank) getIntent().getSerializableExtra("bank");
        mobileService = ApiUtils.MobileService(getApplicationContext());

        loadData();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadData();
            }
        });
    }

    private void loadData() {
//        showPleasewaitDialog();
        swipeRefreshLayout.setRefreshing(true);
        mobileService.getService().enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
//                dissmissPleasewaitDialog();
                swipeRefreshLayout.setRefreshing(false);
                if (response.isSuccessful()) {
                    Response body = response.body();
                    if (body.getData() != null) {
                        Gson gson = new Gson();
                        JsonObject jsonObject = gson.toJsonTree(body).getAsJsonObject();
                        List<Service> listBody = gson.fromJson(jsonObject.getAsJsonArray("data"), new TypeToken<List<Service>>() {
                        }.getType());
                        ServiceAdapter adapter = new ServiceAdapter(listBody, new ServiceAdapter.OnItemClickListener() {
                            @Override
                            public void onItemClick(Service model) {
                                Intent intent = new Intent(getApplicationContext(), SumaryActivity.class);
                                intent.putExtra("bank", bank);
                                intent.putExtra("service", model);
                                startActivity(intent);
                            }
                        });
                        final LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                        recyclerView.setLayoutManager(layoutManager);
                        recyclerView.setAdapter(adapter);
                    }
                } else {
                    showMessage(Static.SOMETHING_WRONG);
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
//                dissmissPleasewaitDialog();
                swipeRefreshLayout.setRefreshing(false);
                showMessage(Static.SOMETHING_WRONG);
            }
        });
    }

    @OnClick(R.id.iv_finish)
    public void onViewClicked() {
        finish();
    }
}
