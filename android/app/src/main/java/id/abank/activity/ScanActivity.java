package id.abank.activity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.zxing.Result;

import id.abank.client.ApiUtils;
import id.abank.client.mobile.MobileService;
import id.abank.model.MyTrxBooking;
import id.abank.util.BaseActivity;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class ScanActivity extends BaseActivity implements ZXingScannerView.ResultHandler {
    private ZXingScannerView mScannerView;

    String TAG = this.getClass().getName();
    MyTrxBooking myTrxBooking;
    MobileService mobileService;

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        mScannerView = new ZXingScannerView(this);   // Programmatically initialize the scanner view
        setContentView(mScannerView);                // Set the scanner view as the content view

        myTrxBooking = (MyTrxBooking) getIntent().getSerializableExtra("myTrxBooking");
        mobileService = ApiUtils.MobileService(getApplicationContext());
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();          // Start camera on resume
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();           // Stop camera on pause
    }

    @Override
    public void handleResult(Result rawResult) {
        // Do something with the result here
        String qrId = rawResult.getText();
        if(checkQrFormat(qrId)){
            showMessage("Barcode valid");
            Intent returnIntent = new Intent();
            returnIntent.putExtra("result", qrId);
            setResult(Activity.RESULT_OK,returnIntent);
            finish();
        }else{
            showMessage("Barcode tidak valid, pastikan scan sesuai bank yang anda booking");
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_CANCELED, returnIntent);
            finish();
        }
//        showMessage(rawResult.getText());
        Log.v(TAG, rawResult.getText()); // Prints scan results
        Log.v(TAG, rawResult.getBarcodeFormat().toString()); // Prints the scan format (qrcode, pdf417 etc.)

        // If you would like to resume scanning, call this method below:
        mScannerView.resumeCameraPreview(this);
    }

    private boolean checkQrFormat(String text){
        try{
            String[] data = text.split("-");
//            if(String.valueOf(myTrxBooking.getBankId()).equals(data[0]) && String.valueOf(myTrxBooking.getServiceId()).equals(data[1])){
            if(String.valueOf(myTrxBooking.getBankId()).equals(data[0])){
                return true;
            }else{
                return false;
            }

        }catch (Exception e){
            return false;
        }
    }
}
