package id.abank.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.abank.R;
import id.abank.adapter.BankAdapter;
import id.abank.client.ApiUtils;
import id.abank.client.mobile.MobileService;
import id.abank.client.model.Response;
import id.abank.model.Bank;
import id.abank.util.BaseActivity;
import id.abank.util.Static;
import retrofit2.Call;
import retrofit2.Callback;

public class PilihBankActivity extends BaseActivity {

    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.bt_1)
    TextView bt1;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    MobileService mobileService;
    public static Activity fa;
    @BindView(R.id.container)
    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pilih_bank);
        ButterKnife.bind(this);
        fa = this;
        tvTitle.setText("PILIH BANK");
        mobileService = ApiUtils.MobileService(getApplicationContext());

        swipeRefreshLayout.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorAccent,
                R.color.colorPrimaryDark);

        loadData();

        bt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), PertanyaanActivity.class));
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadData();
            }
        });
    }

    private void loadData() {
//        showPleasewaitDialog();
        swipeRefreshLayout.setRefreshing(true);
        mobileService.getBank().enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
//                dissmissPleasewaitDialog();
                swipeRefreshLayout.setRefreshing(false);
                if (response.isSuccessful()) {
                    Response body = response.body();
                    if (body.getData() != null) {
                        Gson gson = new Gson();
                        JsonObject jsonObject = gson.toJsonTree(body).getAsJsonObject();
                        List<Bank> listBody = gson.fromJson(jsonObject.getAsJsonArray("data"), new TypeToken<List<Bank>>() {
                        }.getType());
                        BankAdapter adapter = new BankAdapter(listBody, new BankAdapter.OnItemClickListener() {
                            @Override
                            public void onItemClick(Bank model) {
                                Intent intent = new Intent(getApplicationContext(), PertanyaanActivity.class);
                                intent.putExtra("bank", model);
                                startActivity(intent);
                            }
                        });
                        final LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                        recyclerView.setLayoutManager(layoutManager);
                        recyclerView.setAdapter(adapter);
                    }
                } else {
                    showMessage(Static.SOMETHING_WRONG);
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
//                dissmissPleasewaitDialog();
                swipeRefreshLayout.setRefreshing(false);
                showMessage(Static.SOMETHING_WRONG);
            }
        });
    }

    @OnClick({R.id.iv_finish, R.id.tv_title})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_finish:
                finish();
                break;
            case R.id.tv_title:
                break;
        }
    }
}
